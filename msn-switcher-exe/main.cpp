// Win32Project1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "main.h"
#include "Resource.h"

#define SWM_TRAYMSG   WM_APP + 0
#define SWM_SHOW      WM_APP + 1
#define SWM_EXIT      WM_APP + 2
#define SWM_MODE_NORM WM_APP + 3
#define SWM_MODE_DEV  WM_APP + 4

typedef BOOL(WINAPI *PFN_INSTALLHOOK)();
typedef void(WINAPI *PFN_TOGGLEDEV)(BOOL bDev);

// Global Variables
NOTIFYICONDATA niData;
HMODULE hSwitcher;
PFN_TOGGLEDEV ToggleDev;
BOOL bSwitcherDevMode;
HANDLE hMutex;

// Forward declarations of functions included in this code module:
BOOL InitInstance(HINSTANCE, int);
void ShowContextMenu(HWND hWnd);
ULONGLONG GetDllVersion(LPCSTR lpszDllName);

INT_PTR CALLBACK DlgProc(HWND, UINT, WPARAM, LPARAM);

BOOL InstallHook() {
	hSwitcher = LoadLibraryA("msn-switcher-dll.dll");
	if (hSwitcher == NULL) return FALSE;
	ToggleDev = (PFN_TOGGLEDEV)GetProcAddress(hSwitcher, "ToggleDev");
	PFN_INSTALLHOOK installer = (PFN_INSTALLHOOK)GetProcAddress(hSwitcher, "InstallHook");
	if (installer == NULL) return FALSE;
	return installer();
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow) {
	hMutex = CreateMutexA(NULL, FALSE, "{86a39552-5a88-456f-8210-16398ada56a4}");
	if (GetLastError() == ERROR_ALREADY_EXISTS) {
		if (hMutex) CloseHandle(hMutex);
		return 0;
	}

	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	if (!InitInstance(hInstance, nCmdShow)) return 0;
	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_WIN32PROJECT1));

	bSwitcherDevMode = FALSE;
	InstallHook();

	// Main message loop:
	MSG msg;
	while (GetMessage(&msg, nullptr, 0, 0)) {
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow) {
	HWND hWnd = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), NULL, (DLGPROC)DlgProc);
	if (!hWnd) return FALSE;

	// Fill the NOTIFYICONDATA structure and call Shell_NotifyIcon

	// zero the structure - note:	Some Windows funtions require this but
	//								I can't be bothered which ones do and
	//								which ones don't.
	ZeroMemory(&niData, sizeof(NOTIFYICONDATA));

	// get Shell32 version number and set the size of the structure
	//		note:	the MSDN documentation about this is a little
	//				dubious and I'm not at all sure if the method
	//				bellow is correct
	ULONGLONG ullVersion = GetDllVersion("Shell32.dll");
	if (ullVersion >= MAKEDLLVERULL(5, 0, 0, 0))
		niData.cbSize = sizeof(NOTIFYICONDATA);
	else niData.cbSize = NOTIFYICONDATA_V2_SIZE;

	// the ID number can be anything you choose
	niData.uID = 1;

	// state which structure members are valid
	niData.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;

	// load the icon
	niData.hIcon = (HICON)LoadImage(hInstance, MAKEINTRESOURCE(IDI_WIN32PROJECT1),
		IMAGE_ICON, GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON), LR_DEFAULTCOLOR);

	// the window to send messages to and the message to send
	//		note:	the message value should be in the
	//				range of WM_APP through 0xBFFF
	niData.hWnd = hWnd;
	niData.uCallbackMessage = SWM_TRAYMSG;

	// tooltip message
	lstrcpyn(niData.szTip, _T("MSN Switcher"), sizeof(niData.szTip) / sizeof(TCHAR));

	Shell_NotifyIcon(NIM_ADD, &niData);

	// free icon handle
	if (niData.hIcon && DestroyIcon(niData.hIcon))
		niData.hIcon = NULL;

	// call ShowWindow here to make the dialog initially visible
	return TRUE;
}

void ShowContextMenu(HWND hWnd) {
	POINT pt;
	GetCursorPos(&pt);
	HMENU hMenu = CreatePopupMenu();
	HMENU hMenuMode = CreatePopupMenu();
	if (hMenu && hMenuMode) {
		AppendMenuA(hMenu, MF_POPUP, (UINT_PTR)hMenuMode, "Mode");
		AppendMenuA(hMenuMode, (bSwitcherDevMode ? 0 : MF_CHECKED), SWM_MODE_NORM, "Normal");
		AppendMenuA(hMenuMode, (bSwitcherDevMode ? MF_CHECKED : 0), SWM_MODE_DEV, "Development");
		AppendMenuA(hMenu, 0, SWM_SHOW, "Website");
		AppendMenuA(hMenu, 0, NULL, "Version 1.3.2");
		AppendMenuA(hMenu, MF_SEPARATOR, NULL, NULL);
		AppendMenuA(hMenu, 0, SWM_EXIT, "Exit");

		// note: must set window to the foreground or the menu won't disappear when it should
		SetForegroundWindow(hWnd);

		TrackPopupMenu(hMenu, TPM_BOTTOMALIGN, pt.x, pt.y, 0, hWnd, NULL);
		DestroyMenu(hMenu);
	}
}

// Get dll version number
ULONGLONG GetDllVersion(LPCSTR lpszDllName) {
	ULONGLONG ullVersion = 0;
	HINSTANCE hinstDll;
	hinstDll = LoadLibraryA(lpszDllName);
	if (hinstDll) {
		DLLGETVERSIONPROC pDllGetVersion;
		pDllGetVersion = (DLLGETVERSIONPROC)GetProcAddress(hinstDll, "DllGetVersion");
		if (pDllGetVersion) {
			DLLVERSIONINFO dvi;
			HRESULT hr;
			ZeroMemory(&dvi, sizeof(dvi));
			dvi.cbSize = sizeof(dvi);
			hr = (*pDllGetVersion)(&dvi);
			if (SUCCEEDED(hr))
				ullVersion = MAKEDLLVERULL(dvi.dwMajorVersion, dvi.dwMinorVersion, 0, 0);
		}
		FreeLibrary(hinstDll);
	}
	return ullVersion;
}

// Message handler for the app
INT_PTR CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	int wmId, wmEvent;

	switch (message) {
	case SWM_TRAYMSG:
		switch (lParam) {
		case WM_RBUTTONDOWN:
		case WM_CONTEXTMENU:
			ShowContextMenu(hWnd);
		}
		break;
	case WM_SYSCOMMAND:
		if ((wParam & 0xFFF0) == SC_MINIMIZE) {
			ShowWindow(hWnd, SW_HIDE);
			return 1;
		}
		break;
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);

		switch (wmId) {
		case SWM_SHOW:
			ShellExecuteA(NULL, "open", "https://escargot.log1p.xyz", NULL, NULL, SW_SHOWNORMAL);
			break;
		case SWM_MODE_NORM:
			if (ToggleDev) {
				bSwitcherDevMode = false;
				ToggleDev(bSwitcherDevMode);
			}
			break;
		case SWM_MODE_DEV:
			if (ToggleDev) {
				bSwitcherDevMode = true;
				ToggleDev(bSwitcherDevMode);
			}
			break;
		case SWM_EXIT:
			DestroyWindow(hWnd);
			break;
		}
		return 1;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;
	case WM_DESTROY:
		niData.uFlags = 0;
		Shell_NotifyIcon(NIM_DELETE, &niData);
		PostQuitMessage(0);
		break;
	}
	return 0;
}
