#include "stdafx.h"

#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <stdio.h>
#include <windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <WinInet.h>
#include <atlbase.h>
#include <detours.h>

#pragma comment(lib, "detours")
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "wininet.lib")

#ifdef NDEBUG
#define D(fmt, ...)
#else
#define D(fmt, ...) { if (FileDebug) { fprintf(FileDebug, fmt, __VA_ARGS__); fflush(FileDebug); } };
#endif

static BOOL IsAttached = FALSE;
static FILE* FileDebug = NULL;

LRESULT CALLBACK GetMsgProc(int code, WPARAM wParam, LPARAM lParam) {
	return ::CallNextHookEx(NULL, code, wParam, lParam);
}

static HMODULE ModuleFromAddress(PVOID pv) {
	MEMORY_BASIC_INFORMATION mbi;
	return ((::VirtualQuery(pv, &mbi, sizeof(mbi)) != 0)
		? (HMODULE)mbi.AllocationBase : NULL);
}

#pragma data_seg(".HKT")
BOOL sg_DevMode = FALSE;
#pragma data_seg()

BOOL WINAPI InstallHook() {
	HHOOK pHook = SetWindowsHookEx(WH_GETMESSAGE, (HOOKPROC)(GetMsgProc), ModuleFromAddress(GetMsgProc), 0);
	return (NULL != pHook);
}

void WINAPI ToggleDev(BOOL bDev) {
	sg_DevMode = bDev;
}

static BOOL GetProcessHostFullName(LPSTR pszFullFileName) {
	DWORD dwResult = 0;
	::ZeroMemory((PBYTE)pszFullFileName, MAX_PATH);
	if (TRUE != ::IsBadReadPtr((PBYTE)pszFullFileName, MAX_PATH))
		dwResult = ::GetModuleFileNameA(NULL, pszFullFileName, MAX_PATH);
	return (dwResult != 0);
}

static BOOL GetProcessHostName(LPSTR pszFullFileName) {
	BOOL bResult = GetProcessHostFullName(pszFullFileName);
	if (bResult) {
		LPSTR pdest = strrchr(pszFullFileName, '\\');
		if (pdest != NULL)
			strcpy_s(pszFullFileName, MAX_PATH, &pdest[1]);
	}
	return bResult;
}

BOOL IsMSN(LPCSTR name) {
	if (strcmp(name, "msnmsgr.exe") == 0) return TRUE;
	if (strcmp(name, "msmsgs.exe") == 0) return TRUE;
	return FALSE;
}

static const char* MSN_MSNP_ORIG = "messenger.hotmail.com";
static int MSN_MSNP_ORIG_LEN = 21;
static const char* MSN_NEXUS_ORIG = "nexus.passport.com";

static const char* OVERRIDE_SERVER = "m1.escargot.log1p.xyz";
static int OVERRIDE_SERVER_LEN = 21;
static const char* OVERRIDE_DEV_MSNP = "dev-msnp.escargot.log1p.xyz";
static int OVERRIDE_DEV_MSNP_LEN = 27;
static const char* OVERRIDE_DEV_NEXUS = "dev-nexus.escargot.log1p.xyz";

const char* ReplaceServer(const char* s) {
	if (s == NULL) return s;
	if (strcmp(s, MSN_MSNP_ORIG) == 0) {
		if (sg_DevMode) return OVERRIDE_DEV_MSNP;
		return OVERRIDE_SERVER;
	}
	if (strcmp(s, MSN_NEXUS_ORIG) == 0) {
		if (sg_DevMode) return OVERRIDE_DEV_NEXUS;
		return OVERRIDE_SERVER;
	}
	if (strcmp(s, "login.live.com") == 0) {
		if (sg_DevMode) return OVERRIDE_DEV_NEXUS;
		return OVERRIDE_SERVER;
	}
	if (strcmp(s, "loginnet.passport.com") == 0) {
		if (sg_DevMode) return OVERRIDE_DEV_NEXUS;
		return OVERRIDE_SERVER;
	}
	if (strcmp(s, OVERRIDE_DEV_NEXUS) == 0) {
		if (sg_DevMode) return OVERRIDE_DEV_NEXUS;
		return OVERRIDE_SERVER;
	}
	return s;
}

hostent* (WINAPI *real_gethostbyname)(const char*) = gethostbyname;
int (WINAPI *real_getaddrinfo)(PCSTR, PCSTR, const ADDRINFOA*, PADDRINFOA*) = getaddrinfo;
HINTERNET (WINAPI *real_InternetConnectA)(HINTERNET, LPCSTR, INTERNET_PORT, LPCSTR, LPCSTR, DWORD, DWORD, DWORD_PTR) = InternetConnectA;
HINTERNET (WINAPI *real_InternetConnectW)(HINTERNET, LPCWSTR, INTERNET_PORT, LPCWSTR, LPCWSTR, DWORD, DWORD, DWORD_PTR) = InternetConnectW;
BOOL (WINAPI *real_HttpQueryInfoA)(HINTERNET, DWORD, LPVOID, LPDWORD, LPDWORD) = HttpQueryInfoA;
LONG (WINAPI *real_RegQueryValueExA)(HKEY, LPCSTR, LPDWORD, LPDWORD, LPBYTE, LPDWORD) = RegQueryValueExA;

BOOL WINAPI hook_HttpQueryInfoA(HINTERNET a0, DWORD a1, LPVOID a2, LPDWORD a3, LPDWORD a4) {
	BOOL r = real_HttpQueryInfoA(a0, a1, a2, a3, a4);
	if (!r) return r;

	DWORD l = *a3;
	LPVOID data = a2;
	if (l < 50) return r;
	char* p = strstr((char*)data, "\nPassporturls:");
	if (p == NULL) return r;

	// Convert Passporturls to PassportURLs
	p += 1;
	strncpy_s(p, 12, "PassportURLs", _TRUNCATE);
	p[11] = 's';

	return r;
}

HINTERNET WINAPI hook_InternetConnectA(HINTERNET a0, LPCSTR a1, INTERNET_PORT a2, LPCSTR a3, LPCSTR a4, DWORD a5, DWORD a6, DWORD_PTR a7) {
	LPCSTR a1orig = a1;
	a1 = ReplaceServer(a1orig);
	D("InternetConnectA %s -> %s\n", a1orig, a1);
	return real_InternetConnectA(a0, a1, a2, a3, a4, a5, a6, a7);
}

HINTERNET WINAPI hook_InternetConnectW(HINTERNET a0, LPCWSTR a1, INTERNET_PORT a2, LPCWSTR a3, LPCWSTR a4, DWORD a5, DWORD a6, DWORD_PTR a7) {
	USES_CONVERSION;
	LPCSTR a1orig = W2A(a1);
	LPCSTR a1a = ReplaceServer(a1orig);
	a1 = A2W(a1a);
	D("InternetConnectW %s -> %ls\n", a1orig, a1);
	return real_InternetConnectW(a0, a1, a2, a3, a4, a5, a6, a7);
}

hostent* WINAPI __stdcall hook_gethostbyname(const char* a0) {
	const char* a0orig = a0;
	a0 = ReplaceServer(a0orig);
	D("gethostbyname %s -> %s\n", a0orig, a0);
	return real_gethostbyname(a0);
}

int WINAPI hook_getaddrinfo(PCSTR nn, PCSTR sn, const ADDRINFO* a, PADDRINFOA* b) {
	PCSTR nnorig = nn;
	nn = ReplaceServer(nnorig);
	D("getaddrinfo %s -> %s\n", nnorig, nn);
	return real_getaddrinfo(nn, sn, a, b);
}

LONG WINAPI hook_RegQueryValueExA(HKEY a0, LPCSTR a1, LPDWORD a2, LPDWORD a3, LPBYTE a4, LPDWORD a5) {
	LONG ret = real_RegQueryValueExA(a0, a1, a2, a3, a4, a5);
	if (ret != ERROR_SUCCESS) return ret;
	if (!(a1 && a5)) return ret;
	if (strcmp(a1, "Server") != 0) return ret;
	// MSN first does one call with a5 to get the length, then another call to get the value in a4
	if (a4 == NULL) {
		*a5 = OVERRIDE_DEV_MSNP_LEN + 20;
	} else {
		D("RegQueryValueExA %s\n", a1);
		int len;
		const char* s;
		if (sg_DevMode) {
			len = OVERRIDE_DEV_MSNP_LEN;
			s = OVERRIDE_DEV_MSNP;
		} else {
			len = OVERRIDE_SERVER_LEN;
			s = OVERRIDE_SERVER;
		}
		strcpy_s((char*)a4, *a5, s);
		*a5 = len;
	}
	return ret;
}

void AttachHooks() {
	FileDebug = NULL;

	char proc[MAX_PATH + 1];
	BOOL success = GetProcessHostName(proc);
	if (!success) return;
	if (!IsMSN(proc)) return;

	DetourRestoreAfterWith();
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourAttach(&(PVOID&)real_gethostbyname, hook_gethostbyname);
	DetourAttach(&(PVOID&)real_getaddrinfo, hook_getaddrinfo);
	DetourAttach(&(PVOID&)real_InternetConnectA, hook_InternetConnectA);
	DetourAttach(&(PVOID&)real_InternetConnectW, hook_InternetConnectW);
	DetourAttach(&(PVOID&)real_HttpQueryInfoA, hook_HttpQueryInfoA);
	DetourAttach(&(PVOID&)real_RegQueryValueExA, hook_RegQueryValueExA);
	LONG error = DetourTransactionCommit();

	if (error == NO_ERROR) {
		IsAttached = TRUE;
		#ifndef NDEBUG
		fopen_s(&FileDebug, "debug-out.txt", "a");
		#endif
		D("attaching %s\n", proc);
	}
}

void DetachHooks() {
	D("detaching\n");

	if (FileDebug) {
		fclose(FileDebug);
	}

	if (!IsAttached) return;

	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourDetach(&(PVOID&)real_gethostbyname, hook_gethostbyname);
	DetourDetach(&(PVOID&)real_getaddrinfo, hook_getaddrinfo);
	DetourDetach(&(PVOID&)real_InternetConnectA, hook_InternetConnectA);
	DetourDetach(&(PVOID&)real_InternetConnectW, hook_InternetConnectW);
	DetourDetach(&(PVOID&)real_HttpQueryInfoA, hook_HttpQueryInfoA);
	DetourDetach(&(PVOID&)real_RegQueryValueExA, hook_RegQueryValueExA);
	LONG error = DetourTransactionCommit();

	if (error == NO_ERROR) {
		IsAttached = FALSE;
	}
}

INT APIENTRY DllMain(HMODULE hDLL, DWORD Reason, LPVOID Reserved) {
	switch (Reason) {
	case DLL_PROCESS_ATTACH:
		DisableThreadLibraryCalls((HINSTANCE)hDLL);
		AttachHooks();
		break;
	case DLL_PROCESS_DETACH:
		DetachHooks();
		break;
	}

	return TRUE;
}
